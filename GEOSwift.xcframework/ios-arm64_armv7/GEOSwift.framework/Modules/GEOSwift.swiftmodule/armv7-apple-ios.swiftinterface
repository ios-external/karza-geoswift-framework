// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GEOSwift
import Foundation
@_exported import GEOSwift
import Swift
import geos
public enum GeoJSONType : Swift.String, Swift.Codable {
  case point
  case multiPoint
  case lineString
  case multiLineString
  case polygon
  case multiPolygon
  case geometryCollection
  case feature
  case featureCollection
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public protocol WKBInitializable {
  init(wkb: Foundation.Data) throws
}
extension Point : GEOSwift.WKBInitializable {
}
extension LineString : GEOSwift.WKBInitializable {
}
extension Polygon : GEOSwift.WKBInitializable {
}
extension MultiPoint : GEOSwift.WKBInitializable {
}
extension MultiLineString : GEOSwift.WKBInitializable {
}
extension MultiPolygon : GEOSwift.WKBInitializable {
}
extension GeometryCollection : GEOSwift.WKBInitializable {
}
extension Geometry : GEOSwift.WKBInitializable {
}
public enum GEOSwiftError : Swift.Error, Swift.Equatable {
  case invalidJSON
  case invalidGeoJSONType
  case invalidCoordinates
  case mismatchedGeoJSONType
  case tooFewPoints
  case ringNotClosed
  case tooFewRings
  case invalidFeatureId
  case lengthIsZero
  case unexpectedEnvelopeResult(GEOSwift.Geometry)
  case negativeBufferWidth
  public static func == (a: GEOSwift.GEOSwiftError, b: GEOSwift.GEOSwiftError) -> Swift.Bool
}
public protocol SimplicityTestable : GEOSwift.GeometryConvertible {
  func isSimple() throws -> Swift.Bool
}
extension SimplicityTestable {
  public func isSimple() throws -> Swift.Bool
}
extension Point : GEOSwift.SimplicityTestable {
}
extension LineString : GEOSwift.SimplicityTestable {
}
extension Polygon.LinearRing : GEOSwift.SimplicityTestable {
}
extension Polygon : GEOSwift.SimplicityTestable {
}
extension MultiPoint : GEOSwift.SimplicityTestable {
}
extension MultiLineString : GEOSwift.SimplicityTestable {
}
extension MultiPolygon : GEOSwift.SimplicityTestable {
}
extension LineStringConvertible {
  public func distanceFromStart(toProjectionOf point: GEOSwift.Point) throws -> Swift.Double
  public func normalizedDistanceFromStart(toProjectionOf point: GEOSwift.Point) throws -> Swift.Double
  public func interpolatedPoint(withDistance distance: Swift.Double) throws -> GEOSwift.Point
  public func interpolatedPoint(withFraction fraction: Swift.Double) throws -> GEOSwift.Point
}
extension Geometry : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct Envelope : Swift.Hashable {
  public var minX: Swift.Double {
    get
  }
  public var maxX: Swift.Double {
    get
  }
  public var minY: Swift.Double {
    get
  }
  public var maxY: Swift.Double {
    get
  }
  public init(minX: Swift.Double, maxX: Swift.Double, minY: Swift.Double, maxY: Swift.Double)
  public var minXMaxY: GEOSwift.Point {
    get
  }
  public var maxXMaxY: GEOSwift.Point {
    get
  }
  public var minXMinY: GEOSwift.Point {
    get
  }
  public var maxXMinY: GEOSwift.Point {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.Envelope, b: GEOSwift.Envelope) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
extension Envelope : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
public struct MultiLineString : Swift.Hashable {
  public var lineStrings: [GEOSwift.LineString]
  public init(lineStrings: [GEOSwift.LineString])
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.MultiLineString, b: GEOSwift.MultiLineString) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public protocol WKBConvertible {
  func wkb() throws -> Foundation.Data
}
extension Point : GEOSwift.WKBConvertible {
}
extension LineString : GEOSwift.WKBConvertible {
}
extension Polygon.LinearRing : GEOSwift.WKBConvertible {
}
extension Polygon : GEOSwift.WKBConvertible {
}
extension MultiPoint : GEOSwift.WKBConvertible {
}
extension MultiLineString : GEOSwift.WKBConvertible {
}
extension MultiPolygon : GEOSwift.WKBConvertible {
}
extension GeometryCollection : GEOSwift.WKBConvertible {
}
extension Geometry : GEOSwift.WKBConvertible {
}
extension Feature.FeatureId : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension Feature : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension JSON : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension FeatureCollection : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol LineStringConvertible {
  var lineString: GEOSwift.LineString { get }
}
extension LineString : GEOSwift.LineStringConvertible {
  public var lineString: GEOSwift.LineString {
    get
  }
}
extension Polygon.LinearRing : GEOSwift.LineStringConvertible {
  public var lineString: GEOSwift.LineString {
    get
  }
}
extension GeoJSON : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct GeometryCollection : Swift.Hashable {
  public var geometries: [GEOSwift.Geometry]
  public init(geometries: [GEOSwift.GeometryConvertible])
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.GeometryCollection, b: GEOSwift.GeometryCollection) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public enum GEOSError : Swift.Error {
  case unableToCreateContext
  case libraryError(errorMessages: [Swift.String])
  case wkbDataWasEmpty
  case typeMismatch(actual: GEOSwift.GEOSObjectType?, expected: GEOSwift.GEOSObjectType)
  case noMinimumBoundingCircle
}
public struct MultiPoint : Swift.Hashable {
  public var points: [GEOSwift.Point]
  public init(points: [GEOSwift.Point])
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.MultiPoint, b: GEOSwift.MultiPoint) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public enum Geometry : Swift.Hashable {
  case point(GEOSwift.Point)
  case multiPoint(GEOSwift.MultiPoint)
  case lineString(GEOSwift.LineString)
  case multiLineString(GEOSwift.MultiLineString)
  case polygon(GEOSwift.Polygon)
  case multiPolygon(GEOSwift.MultiPolygon)
  case geometryCollection(GEOSwift.GeometryCollection)
  public static func == (a: GEOSwift.Geometry, b: GEOSwift.Geometry) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol Boundable : GEOSwift.GeometryConvertible {
  func boundary() throws -> GEOSwift.Geometry
}
extension Boundable {
  public func boundary() throws -> GEOSwift.Geometry
}
extension Point : GEOSwift.Boundable {
}
extension LineString : GEOSwift.Boundable {
}
extension Polygon.LinearRing : GEOSwift.Boundable {
}
extension Polygon : GEOSwift.Boundable {
}
extension MultiPoint : GEOSwift.Boundable {
}
extension MultiLineString : GEOSwift.Boundable {
}
extension MultiPolygon : GEOSwift.Boundable {
}
public enum JSON : Swift.Hashable {
  case string(Swift.String)
  case number(Swift.Double)
  case boolean(Swift.Bool)
  case array([GEOSwift.JSON])
  case object([Swift.String : GEOSwift.JSON])
  case null
  public var untypedValue: Any {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.JSON, b: GEOSwift.JSON) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
extension JSON : Swift.ExpressibleByStringLiteral {
  public init(stringLiteral value: Swift.String)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias StringLiteralType = Swift.String
  public typealias UnicodeScalarLiteralType = Swift.String
}
extension JSON : Swift.ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Swift.Int)
  public typealias IntegerLiteralType = Swift.Int
}
extension JSON : Swift.ExpressibleByFloatLiteral {
  public init(floatLiteral value: Swift.Double)
  public typealias FloatLiteralType = Swift.Double
}
extension JSON : Swift.ExpressibleByBooleanLiteral {
  public init(booleanLiteral value: Swift.Bool)
  public typealias BooleanLiteralType = Swift.Bool
}
extension JSON : Swift.ExpressibleByArrayLiteral {
  public init(arrayLiteral elements: GEOSwift.JSON...)
  public typealias ArrayLiteralElement = GEOSwift.JSON
}
extension JSON : Swift.ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral elements: (Swift.String, GEOSwift.JSON)...)
  public typealias Key = Swift.String
  public typealias Value = GEOSwift.JSON
}
extension JSON : Swift.ExpressibleByNilLiteral {
  public init(nilLiteral: ())
}
public struct Circle : Swift.Hashable {
  public var center: GEOSwift.Point
  public var radius: Swift.Double
  public init(center: GEOSwift.Point, radius: Swift.Double)
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.Circle, b: GEOSwift.Circle) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public protocol WKTConvertible {
  func wkt() throws -> Swift.String
  func wkt(useFixedPrecision: Swift.Bool, roundingPrecision: Swift.Int32) throws -> Swift.String
}
extension Point : GEOSwift.WKTConvertible {
}
extension LineString : GEOSwift.WKTConvertible {
}
extension Polygon.LinearRing : GEOSwift.WKTConvertible {
}
extension Polygon : GEOSwift.WKTConvertible {
}
extension MultiPoint : GEOSwift.WKTConvertible {
}
extension MultiLineString : GEOSwift.WKTConvertible {
}
extension MultiPolygon : GEOSwift.WKTConvertible {
}
extension GeometryCollection : GEOSwift.WKTConvertible {
}
extension Geometry : GEOSwift.WKTConvertible {
}
public protocol GeometryConvertible {
  var geometry: GEOSwift.Geometry { get }
}
extension Point : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension LineString : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension Polygon.LinearRing : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension Polygon : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension MultiPoint : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension MultiLineString : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension MultiPolygon : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension GeometryCollection : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension Geometry : GEOSwift.GeometryConvertible {
  public var geometry: GEOSwift.Geometry {
    get
  }
}
extension GeometryConvertible {
  public func length() throws -> Swift.Double
  public func distance(to geometry: GEOSwift.GeometryConvertible) throws -> Swift.Double
  public func area() throws -> Swift.Double
  public func nearestPoints(with geometry: GEOSwift.GeometryConvertible) throws -> [GEOSwift.Point]
  public func isEmpty() throws -> Swift.Bool
  public func isRing() throws -> Swift.Bool
  public func isValid() throws -> Swift.Bool
  public func isValidReason() throws -> Swift.String
  public func isValidDetail(allowSelfTouchingRingFormingHole: Swift.Bool = false) throws -> GEOSwift.IsValidDetailResult
  public func isTopologicallyEquivalent(to geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func isDisjoint(with geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func touches(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func intersects(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func crosses(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func isWithin(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func contains(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func overlaps(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func covers(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func isCovered(by geometry: GEOSwift.GeometryConvertible) throws -> Swift.Bool
  public func relate(_ geometry: GEOSwift.GeometryConvertible, mask: Swift.String) throws -> Swift.Bool
  public func relate(_ geometry: GEOSwift.GeometryConvertible) throws -> Swift.String
  public func envelope() throws -> GEOSwift.Envelope
  public func intersection(with geometry: GEOSwift.GeometryConvertible) throws -> GEOSwift.Geometry?
  public func makeValid() throws -> GEOSwift.Geometry
  public func normalized() throws -> GEOSwift.Geometry
  public func convexHull() throws -> GEOSwift.Geometry
  public func minimumRotatedRectangle() throws -> GEOSwift.Geometry
  public func minimumWidth() throws -> GEOSwift.LineString
  public func difference(with geometry: GEOSwift.GeometryConvertible) throws -> GEOSwift.Geometry?
  public func union(with geometry: GEOSwift.GeometryConvertible) throws -> GEOSwift.Geometry
  public func unaryUnion() throws -> GEOSwift.Geometry
  public func pointOnSurface() throws -> GEOSwift.Point
  public func centroid() throws -> GEOSwift.Point
  public func minimumBoundingCircle() throws -> GEOSwift.Circle
  public func polygonize() throws -> GEOSwift.GeometryCollection
  public func buffer(by width: Swift.Double) throws -> GEOSwift.Geometry
  public func simplify(withTolerance tolerance: Swift.Double) throws -> GEOSwift.Geometry
}
extension Collection where Self.Element : GEOSwift.GeometryConvertible {
  public func polygonize() throws -> GEOSwift.GeometryCollection
}
public enum IsValidDetailResult : Swift.Equatable {
  case valid
  case invalid(reason: Swift.String?, location: GEOSwift.Geometry?)
  public static func == (a: GEOSwift.IsValidDetailResult, b: GEOSwift.IsValidDetailResult) -> Swift.Bool
}
public enum GeoJSON : Swift.Hashable {
  case featureCollection(GEOSwift.FeatureCollection)
  case feature(GEOSwift.Feature)
  case geometry(GEOSwift.Geometry)
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: GEOSwift.GeoJSON, b: GEOSwift.GeoJSON) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
public struct FeatureCollection : Swift.Hashable {
  public var features: [GEOSwift.Feature]
  public init(features: [GEOSwift.Feature])
  public static func == (a: GEOSwift.FeatureCollection, b: GEOSwift.FeatureCollection) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Feature : Swift.Hashable {
  public var geometry: GEOSwift.Geometry?
  public var properties: [Swift.String : GEOSwift.JSON]?
  public var id: GEOSwift.Feature.FeatureId?
  public init(geometry: GEOSwift.GeometryConvertible? = nil, properties: [Swift.String : GEOSwift.JSON]? = nil, id: GEOSwift.Feature.FeatureId? = nil)
  public var untypedProperties: [Swift.String : Any]? {
    get
  }
  public enum FeatureId : Swift.Hashable {
    case string(Swift.String)
    case number(Swift.Double)
    public func hash(into hasher: inout Swift.Hasher)
    public static func == (a: GEOSwift.Feature.FeatureId, b: GEOSwift.Feature.FeatureId) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
  }
  public static func == (a: GEOSwift.Feature, b: GEOSwift.Feature) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension Feature.FeatureId : Swift.ExpressibleByStringLiteral {
  public init(stringLiteral value: Swift.String)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias StringLiteralType = Swift.String
  public typealias UnicodeScalarLiteralType = Swift.String
}
extension Feature.FeatureId : Swift.ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Swift.Int)
  public typealias IntegerLiteralType = Swift.Int
}
extension Feature.FeatureId : Swift.ExpressibleByFloatLiteral {
  public init(floatLiteral value: Swift.Double)
  public typealias FloatLiteralType = Swift.Double
}
public struct LineString : Swift.Hashable {
  public let points: [GEOSwift.Point]
  public var firstPoint: GEOSwift.Point {
    get
  }
  public var lastPoint: GEOSwift.Point {
    get
  }
  public init(points: [GEOSwift.Point]) throws
  public init(_ linearRing: GEOSwift.Polygon.LinearRing)
  public static func == (a: GEOSwift.LineString, b: GEOSwift.LineString) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension GeometryCollection : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol ClosednessTestable : GEOSwift.GeometryConvertible {
  func isClosed() throws -> Swift.Bool
}
extension ClosednessTestable {
  public func isClosed() throws -> Swift.Bool
}
extension LineString : GEOSwift.ClosednessTestable {
}
extension Polygon.LinearRing : GEOSwift.ClosednessTestable {
}
extension MultiLineString : GEOSwift.ClosednessTestable {
}
public struct Point : Swift.Hashable {
  public var x: Swift.Double
  public var y: Swift.Double
  public init(x: Swift.Double, y: Swift.Double)
  public static func == (a: GEOSwift.Point, b: GEOSwift.Point) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Polygon : Swift.Hashable {
  public var exterior: GEOSwift.Polygon.LinearRing
  public var holes: [GEOSwift.Polygon.LinearRing]
  public init(exterior: GEOSwift.Polygon.LinearRing, holes: [GEOSwift.Polygon.LinearRing] = [])
  public struct LinearRing : Swift.Hashable {
    public let points: [GEOSwift.Point]
    public init(points: [GEOSwift.Point]) throws
    public func hash(into hasher: inout Swift.Hasher)
    public static func == (a: GEOSwift.Polygon.LinearRing, b: GEOSwift.Polygon.LinearRing) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
  }
  public static func == (a: GEOSwift.Polygon, b: GEOSwift.Polygon) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol WKTInitializable {
  init(wkt: Swift.String) throws
}
extension Point : GEOSwift.WKTInitializable {
}
extension LineString : GEOSwift.WKTInitializable {
}
extension Polygon.LinearRing : GEOSwift.WKTInitializable {
}
extension Polygon : GEOSwift.WKTInitializable {
}
extension MultiPoint : GEOSwift.WKTInitializable {
}
extension MultiLineString : GEOSwift.WKTInitializable {
}
extension MultiPolygon : GEOSwift.WKTInitializable {
}
extension GeometryCollection : GEOSwift.WKTInitializable {
}
extension Geometry : GEOSwift.WKTInitializable {
}
public enum GEOSObjectType {
  case point
  case lineString
  case linearRing
  case polygon
  case multiPoint
  case multiLineString
  case multiPolygon
  case geometryCollection
  public static func == (a: GEOSwift.GEOSObjectType, b: GEOSwift.GEOSObjectType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct MultiPolygon : Swift.Hashable {
  public var polygons: [GEOSwift.Polygon]
  public init(polygons: [GEOSwift.Polygon])
  public static func == (a: GEOSwift.MultiPolygon, b: GEOSwift.MultiPolygon) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension GEOSwift.GeoJSONType : Swift.Equatable {}
extension GEOSwift.GeoJSONType : Swift.Hashable {}
extension GEOSwift.GeoJSONType : Swift.RawRepresentable {}
extension GEOSwift.Polygon : Swift.Encodable {}
extension GEOSwift.Polygon : Swift.Decodable {}
extension GEOSwift.Point : Swift.Encodable {}
extension GEOSwift.Point : Swift.Decodable {}
extension GEOSwift.LineString : Swift.Encodable {}
extension GEOSwift.LineString : Swift.Decodable {}
extension GEOSwift.MultiPoint : Swift.Encodable {}
extension GEOSwift.MultiPoint : Swift.Decodable {}
extension GEOSwift.MultiLineString : Swift.Encodable {}
extension GEOSwift.MultiLineString : Swift.Decodable {}
extension GEOSwift.MultiPolygon : Swift.Encodable {}
extension GEOSwift.MultiPolygon : Swift.Decodable {}
extension GEOSwift.GEOSObjectType : Swift.Equatable {}
extension GEOSwift.GEOSObjectType : Swift.Hashable {}
